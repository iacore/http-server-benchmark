
Command:
```
wrk -t12 -c400 -d30s http://127.0.0.1:3000/
```
Garbage documentation, code not included
fastify hello world (while watching Youtube)
```
Running 30s test @ http://127.0.0.1:3000/
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    16.93ms    7.62ms 189.20ms   96.39%
    Req/Sec     2.01k   369.60     8.76k    86.02%
  719070 requests in 30.08s, 128.24MB read
Requests/sec:  23902.00
Transfer/sec:      4.26MB
```

koa sample.html
```
Running 30s test @ http://127.0.0.1:3000/
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    41.15ms   10.72ms 211.92ms   94.38%
    Req/Sec   816.33    154.16     1.23k    71.44%
  291886 requests in 30.08s, 63.77GB read
Requests/sec:   9704.47
Transfer/sec:      2.12GB
```

elixir plug cowboy sample.html
```
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    23.99ms    3.83ms 145.82ms   92.93%
    Req/Sec     1.39k   121.07     3.00k    89.50%
  496978 requests in 30.10s, 108.58GB read
Requests/sec:  16510.69
Transfer/sec:      3.61GB
```

Pony hello world
```
> wrk -t12 -c400 -d30s http://[::1]:3000/
Running 30s test @ http://[::1]:3000/
  12 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     2.05ms    1.96ms  36.57ms   82.09%
    Req/Sec    18.15k     1.87k   44.74k    79.08%
  6512196 requests in 30.09s, 509.26MB read
Requests/sec: 216396.80
Transfer/sec:     16.92MB
```