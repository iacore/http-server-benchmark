defmodule Sse do
  import Plug.Conn
  use Plug.Router

  plug :match
  plug :dispatch

  get "/" do
    conn
    |> send_file(200, "sample.html")
  end
end

# Run with mix run --no-halt lib/sse.ex
Plug.Adapters.Cowboy.http Sse, [], port: 3000
