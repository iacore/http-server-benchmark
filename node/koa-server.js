const Koa = require('koa');
const app = new Koa();
const fs = require('fs')

const buf = fs.readFileSync("sample.html")
app.use(async ctx => {
    ctx.body = buf;
});

app.listen(3000);
